
//Miguel Pastor Carratalá

#include <iostream>
using namespace std;

int main(){
	
	int n1, n2, n3, average, output;
	
	cout << "First Number? -> ";
	cin >> n1;
	
	cout << endl;
	
	cout << "Second Number? -> ";
	cin >> n2;
	
	cout << endl;
	
	cout << "Third Number? -> ";
	cin >> n3;
	
	cout << endl;
	
	average = (n1 + n2 + n3) / 3;
	
	//If Aproves
	if((n1 >= 4) && (n2 >= 4) && (n3 >= 4) && (average >= 5))
	{
		output = average;
	}
	else if((n1 >= 4) && (n2 >= 4) && (n3 >= 4) && (average <= 5))
	{
		//If Fails #1
		output = 4;
	}
	else
	{
		//If Fails #2
		if(average >= 5)
		{
			output = 3;
		}
		else
		{
			//If Fails #3
			output = average;
		}
	}
	
	cout << "Final Mark -> " << output << endl;
}
